project( mylib )
cmake_minimum_required( VERSION 2.8 FATAL_ERROR )

set( MYPROG_SOURCES myprog.cpp )

find_package( mylib REQUIRED )
message("${MYLIB_INCLUDE_DIRS}")
message("${MYLIB_LIBRARIES}")
include_directories( ${MYLIB_INCLUDE_DIRS} )
add_executable( myprog ${MYPROG_SOURCES} )
target_link_libraries( myprog ${MYLIB_LIBRARIES} )
