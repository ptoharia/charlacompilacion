
namespace mylib
{

  class MyClass
  {
  public:
    MyClass( void );
    float getAttr1( void ) const;
    int getAttr2( void ) const;
    void setAttr1( float attr1 );
    void setAttr2( int attr2 );

  protected:
    float _attr1;
    int _attr2;

  };


}
