#include "myclass.h"

namespace mylib
{

  MyClass::MyClass( void )
    : _attr1( .0f )
    , _attr2( 0 )
  {
  }
  float MyClass::getAttr1( void ) const
  {
    return _attr1;
  }
  int MyClass::getAttr2( void ) const
  {
    return _attr2;
  }
  void MyClass::setAttr1( float attr1 )
  {
    _attr1 = attr1;
  }
  void MyClass::setAttr2( int attr2 )
  {
    _attr2 = attr2;
  }

}
